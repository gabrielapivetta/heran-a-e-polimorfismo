#include "circulo.hpp"
#include <iostream>
#include <math.h>

Circulo:Circulo() {
    tipo = "Circulo";
    raio = 1.0f;
}

Circulo::Circulo(float raio){
    tipo = "Circulo";
    set_raio(raio);
}

Circulo::~Circulo(){}

void Circulo::set_raio(float raio){
    if(raio <= 0)
        throw(1);
    else
        this->raio = raio;
}

float Circulo::get_raio(){
    return raio;
}

float Circulo::calcula_area(){
    return (M_PI * raio * raio);
}

float Circulo::calcula_perimetro(){
    return (2 * M_PI * raio);
}
