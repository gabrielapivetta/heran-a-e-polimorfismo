#include "pentagono.hpp"
#include <iostream>

Pentagono::Pentagono(){
	tipo = "Pentagono";
	base = 1.0f;
	altura = 1.0f;
}

Pentagono::Pentagono(float base, float altura){
	tipo = "Pentagono";
	set_base(base);
	set_altura(altura);
}

Pentagono::~Pentagono(){}

float Pentagono::calcula_area(){
	return (5*base*altura)/2;
}

float Pentagono::calcula_perimetro(){
	return 5*base;
}
