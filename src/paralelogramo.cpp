#include "paralelogramo.hpp"
#include <iostream>

Paralelogramo::Paralelogramo(){
	tipo = "Paralelogramo";
	base = 1.0f;
	altura = 1.0f
}

Paralelogramo::Paralelogramo(float base, float altura){
	tipo = "Paralelogramo";
	set_base(base);
	set_altura(altura);
}

Paralelogramo::~Paralelogramo(){}
