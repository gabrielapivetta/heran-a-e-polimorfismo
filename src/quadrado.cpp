#include "quadrado.hpp"
#include <iostream>

Quadrado::Quadrado(){
	tipo = "Quadrado";
	lado = 1.0f;
}

Quadrado::Quadrado(float lado){
	tipo = "Quadrado";
	set_lado(lado);
}

Quadrado::~Quadrado(){}

void Quadrado::set_lado(float lado){
    if(lado <= 0)
        throw(1);
    else
        this->lado = lado;
}
float Quadrado::get_lado(){
    return lado;
}
