#include "formageometrica.hpp"
#include "quadrado.hpp"
#include "triangulo.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"
#include <iostream>
#include <string>
#include <vector.h>

using namespace std;

int main(int argc, char ** argv){
	vector<FormaGeometrica*> formasGeometricas;
	
	Quadrado * quadrado1 = new Quadrado(4.0f);
	formasGeometricas.push_back(quadrado1);
	Triangulo * triangulo1 = new Triangulo(3.0f, 1.5f);
	formasGeometricas.push_back(triangulo1);
	Circulo * circulo1 = new Circulo(2.5f);
	formasGeometricas.push_back(circulo1);
	Paralelogramo * paralelogramo1 = new Paralelogramo(2.0f, 4.0f);
	formasGeometricas.push_back(paralelogramo1);
	Pentagono * pentagono1 = new Pentagono(3.0f, 2.0f);
	formasGeometricas.push_back(pentagono1);
	Hexagono * hexagono1 = new Hexagono(2.0f);
	formasGeometricas.push_back(hexagono1);

	for(auto x:formasGeometricas){
        cout << x.get_tipo() << endl
        cout << x.calcula_area() << endl
        cout << x.calcula_perimetro() << endl
    }
	
	

return 0;
}

