#include "hexagono.hpp"
#include <iostream>
#include <math.h>

Hexagono::Hexagono(){
	tipo = "Hexagono";
	base = 1.0f;
}

Hexagono::Hexagono(float base){
	tipo = "Hexagono";
	set_base(base);
}

Hexagono::~Hexagono(){}

float Hexagono::calculo_area(){
	return (3*base*base*(sqrt(3)))/2;
}

float Hexagono::calcula_perimetro(){
	return 6*base;
}
