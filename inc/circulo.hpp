#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formageometrica.hpp"

class Circulo : public FormaGeometrica{
private:
	float raio;
	
public:
	Circulo();
	Circulo(float raio);
	~Circulo();

	float calcula_area();
    float calcula_perimetro();
};

#endif
