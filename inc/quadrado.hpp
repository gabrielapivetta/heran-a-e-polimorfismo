#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formageometrica.hpp"

class Quadrado: public FormaGeometrica{
private:
	float lado;

public:
	Quadrado();
	Quadrado(float lado);
	~Quadrado();
	void set_lado(float lado);
	float get_lado();
};

#endif
